$(document).ready(function () {

    //Preloader
    $(window).load(function () {
        $("#loader").fadeOut();
        $("#mask").delay(1000).fadeOut("slow");
    });

    //Adding fixed position to header
    $(document).scroll(function () {
        if ($(document).scrollTop() >= $("#home").height() - 30) {
            $('.navbar').addClass('navbar-fixed-top');
            $('html').addClass('has-fixed-nav');
        } else {
            $('.navbar').removeClass('navbar-fixed-top');
            $('html').removeClass('has-fixed-nav');
        }
    });

    //Navigation Scrolling
    $('a[href*=#]').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                if (this.hash.slice(1) == 'about')
                { var ht = $("#header").height(); }
                else { var ht = 35; }
                $('html,body').animate({
                    scrollTop: target.offset().top - ht
                }, 1500);
                return false;
            }
        }
    });

    $('a[href=#about]').click(function () {
        //alert("in about");
        var iScrollPos = 0;
        var iCurScrollPos = $(window).scrollTop();
        if (iCurScrollPos > iScrollPos) {
            var targetdiv = $("#aboutus");

            $('html,body').animate({
                scrollTop: targetdiv.offset().top - 130
            }, 1500);
        } else {
            $("#aboutus").css('margin-top', '60px');
        }

    });

    $('a[href=#contact-form]').click(function () {
        //alert(this.id);
        $('#hdnservice').val(this.id);
    });

    //Close navbar on click
    $('.nav a').on('click', function () {
        if ($(window).width() < 768) {
            $(".navbar-toggle").click();
        }
    });

    //Nav Selection
    $('#nav').onePageNav({
        currentClass: 'active',
        scrollOffset: 50
    });

    //Home Text Slider
    $('.home-slider').flexslider({
        animation: "slide",
        directionNav: false,
        controlNav: false,
        direction: "vertical",
        slideshowSpeed: 4000,
        animationSpeed: 500,
        smoothHeight: true,
        useCSS: true
    });

    //Elements animation
    $('.animated').appear(function () {
        var element = $(this);
        var animation = element.data('animation');
        var animationDelay = element.data('delay');
        if (animationDelay) {
            setTimeout(function () {
                element.addClass(animation + " visible");
                element.removeClass('hiding');
                if (element.hasClass('counter')) {
                    element.children('.value').countTo();
                }
            }, animationDelay);
        } else {
            element.addClass(animation + " visible");
            element.removeClass('hiding');
            if (element.hasClass('counter')) {
                element.children('.value').countTo();
            }
        }
    }, { accY: -150 });

    //Portfolio filters
    $('#portfolio-grid').mixitup({
        effects: ['fade', 'scale'],
        easing: 'snap'
    });

    //Portfolio project slider
    function initProjectSlider() {
        $('.project-slider').flexslider({
            prevText: "",
            nextText: "",
            useCSS: false,
            animation: "slide"
        });
    };

    //Portfolio Project Loading
    $('.open-project').click(function () {
        $('html, body').animate({ scrollTop: $("#project-extended").offset().top }, 900);
        var projectUrl = $(this).attr("href");

        $('#project-content').animate({ opacity: 0 }, 400, function () {
            $("#project-content").load(projectUrl);
            $('#project-content').delay(400).animate({ opacity: 1 }, 400);
        });

        //Project Page Open
        $('#project-extended').slideUp(600, function () {
            $('#project-extended').addClass('open');
        }).delay(500).slideDown(600, function () {
            $('#project-content').fadeIn('slow', function () {
                if ($('.project-slider').length > 0) {
                    initProjectSlider();
                }
            });
        });

        return false;

    });

    //Project Page Close
    $('#close-project').click(function (event) {
        $('#project-content').animate({ opacity: 0 }, 400, function () {
            $('#project-extended').delay(400).slideUp(400).removeClass('open');
            $('html, body').animate({ scrollTop: $("#solutions").offset().top - 60 }, 900);
        });
        return false;
    });

    //Princing table selection
    $('.plan').click(function () {
        $('.plan').removeClass('selected');
        $(this).addClass('selected');
    });

    //Testimonials slider
    $('.testimonials-slider').flexslider({
        animation: "slide",
        directionNav: false,
        slideshowSpeed: 4000,
        useCSS: false
    });

    //Clients carousel
    $("#clients-carousel").owlCarousel({
        items: 4,
        itemsDesktop: [1000, 4],
        itemsDesktopSmall: [900, 3],
        itemsTablet: [600, 2],
        itemsMobile: false,
        autoPlay: 4000,
        pagination: false
    });

    //https://www.google.com/maps/ms?msid=214515722147110637933.0004fa4b741f7b140b035&msa=0&ll=19.193378,72.9701&spn=0.00151,0.002642&iwloc=0004fa4b80aff69712519

    //Google Maps
    function initMap() {
        var myLatlng = new google.maps.LatLng(19.193378, 72.9701); // <- Your latitude and longitude
        var styles = [
      {
          "featureType": "water",
          "stylers": [
        {
            "color": "#eee"
        },
        {
            "visibility": "on"
        }
        ]
      },
      {
          "featureType": "landscape",
          "stylers": [
        {
            "color": "#f2f2f2"
        }
        ]
      },
      {
          "featureType": "road",
          "stylers": [
        {
            "saturation": -100
        },
        {
            "lightness": 45
        }
        ]
      },
      {
          "featureType": "road.highway",
          "stylers": [
        {
            "visibility": "simplified"
        }
        ]
      },
      {
          "featureType": "road.arterial",
          "elementType": "labels.icon",
          "stylers": [
        {
            "visibility": "on"
        }
        ]
      },
      {
          "featureType": "administrative",
          "elementType": "labels.text.fill",
          "stylers": [
        {
            "color": "#444444"
        }
        ]
      },
      {
          "featureType": "transit",
          "stylers": [
        {
            "visibility": "on"
        }
        ]
      },
      {
          "featureType": "poi",
          "stylers": [
        {
            "visibility": "off"
        }
        ]
      }
     ]

        var mapOptions = {
            zoom: 16,
            center: myLatlng,
            mapTypeControl: false,
            disableDefaultUI: true,
            zoomControl: false,
            scrollwheel: false,
            styles: styles
        }
        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'We are located here!'
        });
    }

    google.maps.event.addDomListener(window, 'load', initMap);

    //Contact form validation and submit with ajax
    $('#contact-us').validate({
        errorPlacement: function (error, element) { },
        highlight: function (element, errorClass) {
            $(element).parent().removeClass('success').addClass('error');
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().removeClass('error').addClass('success');
        },
        rules: {
            fullname: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true
            },
            message: {
                required: true
            }
        },
        submitHandler: function (form) {
            var url = $(form).attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: $(form).serialize(), // serializes the form's elements.
                success: function (data) {
                    //alert(data);
                    $('#form-sent1').slideDown(400); // show response from the php script.
                    $(form)[0].reset();
                }
            });
        }
    });


    //Contact form validation and submit with ajax
    $('#resume-form').validate({
        errorPlacement: function (error, element) { },
        highlight: function (element, errorClass) {
            $(element).parent().removeClass('success').addClass('error');
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().removeClass('error').addClass('success');
        },
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                number: true
            },
            qualification: {
                required: true
            },
            exp: {
                required: true
            },
            resume: {
                required: true
            }
        },
        submitHandler: function (form) {
            var url = $(form).attr('action');
            $.ajax({
                type: "POST",
                url: url,
                //data: $(form).serialize(), // serializes the form's elements.
                data: new FormData($(form)[0]),
                processData: false,
                contentType: false,
                success: function (data) {
                    //alert(data);
                    $('#form-sent2').slideDown(400); // show response from the php script.
                    $(form)[0].reset();
                }
            });
        }
    });
});

var map;
        // svg path for target icon
        var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
        // svg path for plane icon
        /*var planeSVG = "M19.671,8.11l-2.777,2.777l-3.837-0.861c0.362-0.505,0.916-1.683,0.464-2.135c-0.518-0.517-1.979,0.278-2.305,0.604l-0.913,0.913L7.614,8.804l-2.021,2.021l2.232,1.061l-0.082,0.082l1.701,1.701l0.688-0.687l3.164,1.504L9.571,18.21H6.413l-1.137,1.138l3.6,0.948l1.83,1.83l0.947,3.598l1.137-1.137V21.43l3.725-3.725l1.504,3.164l-0.687,0.687l1.702,1.701l0.081-0.081l1.062,2.231l2.02-2.02l-0.604-2.689l0.912-0.912c0.326-0.326,1.121-1.789,0.604-2.306c-0.452-0.452-1.63,0.101-2.135,0.464l-0.861-3.838l2.777-2.777c0.947-0.947,3.599-4.862,2.62-5.839C24.533,4.512,20.618,7.163,19.671,8.11z";*/

        AmCharts.ready(function () {
            map = new AmCharts.AmMap();
            map.pathToImages = "http://www.ammap.com/lib/3/images/";

            var dataProvider = {
                mapVar: AmCharts.maps.worldLow
            };

            map.areasSettings = {
                unlistedAreasColor: "#FCB185"
            };

            map.imagesSettings = {
                color: "#FE660A",
                rollOverColor: "#CC0000",
                selectedColor: "#283264"
            };

            map.linesSettings = {
                color: "#283264",
                alpha: 1.0
            };

            map.zoomControl = { buttonFillColor: "#FF660A" };

            map.backgroundZoomsToTop= true;
	        map.linesAboveImages= true

            // INDIAx
            var india = {
                id: "IN",
                color: "#000000",
                svgPath: targetSVG,
                title: "India",
                latitude: 21.0000,
                longitude: 78.0000,
                scale: 2,
                zoomLevel: .75,
                zoomLongitude: 73.050779,
                zoomLatitude: 19.619413,

                lines: [
            {
                latitudes: [39.55, 21.0000],
                longitudes: [116.25, 78.0000]
            },
            {
                latitudes: [45.25, 21.00],
                longitudes: [141.40, 78.0000]
            },
            {
                latitudes: [-27.00, 21.000],
                longitudes: [133.00, 78.0000]
            },
            {
                latitudes: [16.0, 21.000],
                longitudes: [106.0, 78.0000]
            },
            {
                latitudes: [54.00, 21.000],
                longitudes: [-2.00, 78.0000]
            },
            {
                latitudes: [1.14, 21.000],
                longitudes: [103.55, 78.0000]
            },
            {
                latitudes: [55.45, 21.000],
                longitudes: [37.36, 78.0000]
            },
            {
                latitudes: [48.48, 21.000],
                longitudes: [2.20, 78.0000]
            },
            {
                latitudes: [1.25, 21.000],
                longitudes: [36.55, 78.0000]
            },
            {
                latitudes: [48.8, 21.000],
                longitudes: [11.35, 78.0000]
            },
            {
                latitudes: [26.12, 21.000],
                longitudes: [28.4, 78.0000]
            },
            {
                latitudes: [22.20, 21.000],
                longitudes: [114.20, 78.0000]
            },
            {
                latitudes: [60.10, 21.000],
                longitudes: [25.0, 78.0000]
            },
            {
                latitudes: [-34.35, 21.000],
                longitudes: [-58.22, 78.0000]
            },
            {
                latitudes: [50.422, 21.000],
                longitudes: [30.5367, 78.0000]
            },
            {
                latitudes: [40.43, 21.000],
                longitudes: [-74, 78.0000]
            }

        ]

                /*images: [{
                label: "Flights from London",
                svgPath: planeSVG,
                left: 100,
                top: 45,
                labelShiftY:5,
                color: "#CC0000",
                labelColor: "#CC0000",
                labelRollOverColor: "#CC0000",
                labelFontSize: 20},
                ]*/
            };
            // cities
            var cities = [
        india,

        {
            svgPath: targetSVG,
            title: "China",
            latitude: 39.55,
            longitude: 116.25
        },
        {
            svgPath: targetSVG,
            title: "Japan",
            latitude: 45.25,
            longitude: 141.40
        },
        {
            svgPath: targetSVG,
            title: "Australia",
            latitude: -27.00,
            longitude: 133.00
        },
        {
            svgPath: targetSVG,
            title: "Vietnam",
            latitude: 16.0,
            longitude: 106.0
        },
        {
            svgPath: targetSVG,
            title: "United Kingdom",
            latitude: 54.00,
            longitude: -2.00
        },
        {
            svgPath: targetSVG,
            title: "Singapore",
            latitude: 1.14,
            longitude: 103.55
        },
        {
            svgPath: targetSVG,
            title: "Russia",
            latitude: 55.45,
            longitude: 37.36
        },
        {
            svgPath: targetSVG,
            title: "Paris",
            latitude: 48.48,
            longitude: 2.20
        },
        {
            svgPath: targetSVG,
            title: "Nairobi",
            latitude: 1.25,
            longitude: 36.55
        },
        {
            svgPath: targetSVG,
            title: "Germany",
            latitude: 48.8,
            longitude: 11.35
        },
        {
            svgPath: targetSVG,
            title: "South Africa",
            latitude: 26.12,
            longitude: 28.4
        },
        {
            svgPath: targetSVG,
            title: "Hong Kong",
            latitude: 22.20,
            longitude: 114.11
        },
        {
            svgPath: targetSVG,
            title: "Finland",
            latitude: 60.10,
            longitude: 25.0
        },
        {
            svgPath: targetSVG,
            title: "Argentina",
            latitude: -34.35,
            longitude: -58.22
        },
        {
            svgPath: targetSVG,
            title: "Kiev",
            latitude: 50.4422,
            longitude: 30.5367
        },
        {
            svgPath: targetSVG,
            title: "New York",
            latitude: 40.43,
            longitude: -74
        }];


            dataProvider.linkToObject = india;
            dataProvider.images = cities;
            map.dataProvider = dataProvider;

            map.write("mapdiv");
            $('g[transform="translate(10,10)"]').css({'display':'none'}); //Hack CSS of Map
            $('a[title*="Interactive JavaScript maps"]').css({'filter':'alpha(opacity=0)', 'zoom':'1', 'opacity':'0.0'}); //Hack CSS of Map
        });

        